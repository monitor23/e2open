package ObjectRepositories;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class E2OpenHome {
	WebDriver driver;
	WebDriverWait wait;

	@SuppressWarnings("deprecation")
	public E2OpenHome(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 180);
		PageFactory.initElements(driver, this);
	}

	public void ViewHelp() throws InterruptedException {
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(
				"#dashboardnav > table:nth-child(3) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2) > a:nth-child(1)")))
				.click();
		String parentHandle = driver.getWindowHandle();
		driver.findElement(By.cssSelector(
				"#dashboardnav > table:nth-child(3) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2) > a:nth-child(1)"))
				.click();
		for (String winHandle : driver.getWindowHandles()) {
			driver.switchTo().window(winHandle);
		}
		Thread.sleep(4000);
		driver.close();
		driver.switchTo().window(parentHandle);
	}

	public void ViewProcessManager() throws InterruptedException {
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(
				"#left > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2) > div:nth-child(1) > div:nth-child(1) > table:nth-child(1) > tbody:nth-child(1) > tr:nth-child(1) > td:nth-child(1) > a:nth-child(1) > img:nth-child(1)")))
				.click();
		Thread.sleep(2000);
		driver.switchTo().frame("i2ui_shell_top");
		String uName = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("shellUsername"))).getText();
		System.out.println("----- Page Details -----");
		System.out.println("Title : " + driver.getTitle());
		System.out.println("----- User Details -----");
		System.out.println(uName);
		Thread.sleep(4000);
		wait.until(ExpectedConditions
				.visibilityOfElementLocated(By.cssSelector("#shellActions > b:nth-child(2) > a:nth-child(1)"))).click();
		driver.switchTo().defaultContent();

	}

	public void Logout() throws InterruptedException {
		Thread.sleep(2000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(
				"#dashboardnav > table:nth-child(3) > tbody:nth-child(1) > tr:nth-child(2) > td:nth-child(2) > a:nth-child(2)")))
				.click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".eto-btn"))).click();
		Thread.sleep(3000);
		driver.quit();
	}

}
