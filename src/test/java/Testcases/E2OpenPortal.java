package Testcases;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ObjectRepositories.E2OpenHome;
import ObjectRepositories.E2OpenLogin;

public class E2OpenPortal {
	WebDriver driver;

	@BeforeTest
	public void Initializtion() {
		System.setProperty("webdriver.gecko.driver", "C:\\MyWorkspace\\geckodriver.exe");
		System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE, "true");
		System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE, "C:\\SeleniumLogs\\seleniumlogs.txt");
		driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("https://alcatel-lucent.e2open.com/aluprod_pl/portal?");

	}

	@Test
	public void E2Open() throws InterruptedException {

		E2OpenLogin eLogin = new E2OpenLogin(driver);
		eLogin.CookieandPrivacyNotification();
		eLogin.WaitFunction();
		eLogin.Username().sendKeys("manu.uppal@netlink-group.com");
		eLogin.Password().sendKeys("June2021+");
		eLogin.Login().click();
		E2OpenHome eHome = new E2OpenHome(driver);
		eHome.ViewProcessManager();
		// eHome.ViewHelp();
		eHome.Logout();
	}

}
